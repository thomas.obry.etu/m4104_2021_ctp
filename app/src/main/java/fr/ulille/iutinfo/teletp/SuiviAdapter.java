package fr.ulille.iutinfo.teletp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SuiviAdapter extends RecyclerView.Adapter<SuiviAdapter.ViewHolder>/* TODO Q6.a */ {
    protected SuiviViewModel model;

    public SuiviAdapter(SuiviViewModel model) {
        this.model = model;
    }

    // TODO Q6.a
    class ViewHolder extends RecyclerView.ViewHolder {
        private final View itemView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;
        }

        public TextView getQuestionView() {
            return itemView.findViewById(R.id.question);
        }

        public CheckBox getCheckBox() {
            return itemView.findViewById(R.id.checkBox);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.question_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            String question = model.getQuestions()[position];
            holder.getQuestionView().setText(question);
    }

    @Override
    public int getItemCount() {
        return model.getQuestions().length;
    }

    // TODO Q7
    public void check(int position){
        Integer next = model.getNextQuestion();

    }
}

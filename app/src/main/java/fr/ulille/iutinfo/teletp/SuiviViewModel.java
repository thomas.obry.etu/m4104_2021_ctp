package fr.ulille.iutinfo.teletp;

import android.app.Application;
import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class SuiviViewModel extends AndroidViewModel {

    private MutableLiveData<String> liveLocalisation;
    private MutableLiveData<String> liveUsername;
    // TODO Q2.a
    private MutableLiveData<Integer> nextQuestion;
    private String[] questions;

    public SuiviViewModel(Application application) {
        super(application);
        liveLocalisation = new MutableLiveData<>();
        liveUsername = new MutableLiveData<>();
        nextQuestion = new MutableLiveData<>();
    }

    public LiveData<String> getLiveUsername() {
        return liveUsername;
    }

    public void setUsername(String username) {
        liveUsername.setValue(username);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Username : " + username, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getUsername() {
        return liveUsername.getValue();
    }

    public LiveData<String> getLiveLocalisation() {
        return liveLocalisation;
    }

    public void setLocalisation(String localisation) {
        liveLocalisation.setValue(localisation);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Localisation : " + localisation, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getLocalisation() {
        return liveLocalisation.getValue();
    }
    
    // TODO Q2.a
    public void initQuestions(Context context){
        this.questions = getApplication().getResources().getStringArray(R.array.list_questions);
    }

    public String[] getQuestions(){
        return this.questions;
    }

    public String getQuestion(int position){
        if(position >= 0 && position < questions.length){
            return this.questions[position];
        }
        return null;
    }

    public LiveData<Integer> getLiveNextQuestion(){
        return nextQuestion;
    }

    public void setNextQuestion(Integer nextQuestion){
        this.nextQuestion.setValue(nextQuestion);
    }

    public void incrNextQuestion(){
        setNextQuestion(getNextQuestion()+1);
    }

    public Integer getNextQuestion(){
        return nextQuestion.getValue();
    }
}

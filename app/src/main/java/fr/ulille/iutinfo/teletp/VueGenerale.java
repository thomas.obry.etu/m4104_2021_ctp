package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    protected String salle;
    protected String poste;
    protected String DISTANCIEL;
    // TODO Q2.c
    private SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        this.DISTANCIEL = getActivity().getResources().getStringArray(R.array.list_salles)[0];
        this.poste = "";
        this.salle = this.DISTANCIEL;
        // TODO Q2.c
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);

        // TODO Q4
        Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        spSalle.setAdapter(ArrayAdapter.createFromResource(getContext(), R.array.list_salles, android.R.layout.simple_spinner_item));

        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        spPoste.setAdapter(ArrayAdapter.createFromResource(getContext(), R.array.list_postes, android.R.layout.simple_spinner_item));

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            model.setUsername(((TextView) getActivity().findViewById(R.id.tvLogin)).getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        update();
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                salle = (String) parent.getItemAtPosition(position);
                update();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                poste = (String) parent.getItemAtPosition(position);
                update();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        // TODO Q9
    }

    // TODO Q5.a
    public void update(){
        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        if(this.salle.equals(this.DISTANCIEL)){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            model.setLocalisation("Distanciel");
        }else{
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            model.setLocalisation(this.salle + " : " + this.poste);
        }
    }

    // TODO Q9
}